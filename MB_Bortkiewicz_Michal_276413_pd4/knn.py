import numpy as np
import pandas as pd
from knn_aux import find_point_nearest_neigbours, classify_point


def knn(X, y, Z, k):
    assert type(k) == int, "k is not an int"
    assert k <= len(X), "k > n"
    assert k >= 1, "k < 1"
    assert isinstance(X, (pd.DataFrame, np.ndarray)), "X in not ndarray or DataFrame"
    assert isinstance(y, (pd.DataFrame, np.ndarray)), "y in not ndarray or DataFrame"
    assert isinstance(Z, (pd.DataFrame, np.ndarray)), "Z in not ndarray or DataFrame"

    if isinstance(X, pd.DataFrame):
        X = X.values
    if isinstance(y, pd.DataFrame):
        y = y.values
    if isinstance(Z, pd.DataFrame):
        Z = Z.values


    Z_knn = [find_point_nearest_neigbours(X, y, point, k) for point in Z]
    classes = [classify_point(point_neighbors) for point_neighbors in Z_knn]
    # return y_pred in shape (m, 1)
    # return np.asanyarray(classes).reshape((len(classes), 1))
    return pd.DataFrame(classes)



