import numpy as np
from scipy import stats
import pandas as pd
import os
from collections import Counter
from random import seed
from random import randrange
import copy
from sklearn.datasets import make_classification
import seaborn as sns
import matplotlib.pyplot as plt

# TODO: Dorzucic asserty!!!
# TODO: moze zamien na wlasna odleglosc
def find_point_nearest_neigbours(X, y, point, k):
    """
    Find classes (labels) for k nearest neighbors
    :param X: training set features
    :param y: training set labels
    :param point: test point features
    :param k: number of neighbors
    :return: list of k neighbour labels
    """

    euclidean_distances = [np.linalg.norm(np.subtract(a, point)) for a in X]
    k_nearest = sorted(zip(euclidean_distances, np.squeeze(y)), key=lambda x: x[0])[:k]
    return list(zip(*k_nearest))[1]

def classify_point(point_neighbors):
    """
    Classify paint as majority of its neighbors
    :param point_neighbors: neighbors of point
    :return: label
    """
    # return stats.mode(point_neighbors[:][1])
    occurrences = Counter(point_neighbors)
    max_occ = max(occurrences.values())
    classes = [k for k, v in occurrences.items() if v == max_occ]
    return np.random.choice(classes, 1)

def load_data_labels(folder_path, data_name, labels_name):
    """
    Load data for benchmark or test purposes
    :param folder_path: path to folder with data
    :param data_name: name of features file
    :param labels_name: name of labels file
    :return: data, labels as dataframes
    """
    assert os.path.isdir(folder_path), "folder_path is not valid path"
    assert os.path.isfile(os.path.join(folder_path, data_name)), "there is not file called data_name in folder_path"
    assert os.path.isfile(os.path.join(folder_path, labels_name)), "there is not file called labels_name in folder_path"

    data = pd.read_csv(os.path.join(folder_path, data_name), header=None)
    labels = pd.read_csv(os.path.join(folder_path, labels_name), header=None)
    return data, labels


def cross_validation_split(dataset, labels, folds=5):
    """
    Cross validation split
    :param dataset: features
    :param labels: labels (classes)
    :param folds: number of folds
    :return: list of [fold, fold_label] of len folds
    """
    assert isinstance(dataset, pd.DataFrame), "dataset is not pandas DataFrame"
    assert isinstance(labels, pd.DataFrame), "labels is not pandas DataFrame"

    dataset_split = list()
    # jezeli dataset jest w postaci dataframe - trzeba bedzie sprawdzic
    dataset_copy = dataset.values.tolist()
    labels_copy = labels.values.tolist()
    # utrata maksymalnie folds-1 probek
    fold_size = int(len(dataset) / folds)
    for i in range(folds):
        fold = list()
        fold_label = list()
        while len(fold) < fold_size and len(dataset_copy) > 0:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
            fold_label.append(labels_copy.pop(index))
        dataset_split.append([pd.DataFrame(fold), pd.DataFrame(fold_label)])
    return dataset_split


def calculate_error(y_test, y_pred):
    """
    Calculate simple error rate
    :param y_test:
    :param y_pred:
    :return:
    """
    assert isinstance(y_test, (pd.DataFrame, np.ndarray)), "y_test in not ndarray or DataFrame"
    assert isinstance(y_pred, (pd.DataFrame, np.ndarray)), "y_pred in not ndarray or DataFrame"

    if isinstance(y_test, pd.DataFrame):
        y_test = y_test.values
    if isinstance(y_pred, pd.DataFrame):
        y_pred = y_pred.values

    error = 0
    for i in range(y_test.shape[0]):
        if y_test[i] != y_pred[i]:
            error += 1
    return error/y_test.shape[0]


def calculate_accuracy(y_test, y_pred):
    """
    Calculate accuracy
    :param y_test:
    :param y_pred:
    :return:
    """
    assert isinstance(y_test, (pd.DataFrame, np.ndarray)), "y_test in not ndarray or DataFrame"
    assert isinstance(y_pred, (pd.DataFrame, np.ndarray)), "y_pred in not ndarray or DataFrame"

    if isinstance(y_test, pd.DataFrame):
        y_test = y_test.values
    if isinstance(y_pred, pd.DataFrame):
        y_pred = y_pred.values

    acc = 0
    for i in range(y_test.shape[0]):
        if y_test[i] == y_pred[i]:
            acc += 1
    return acc/y_test.shape[0]



def create_2D_set(n_samples=1000, n_classes=3, n_clusters=1):
    """
    Generate simple dataset with two features
    :param n_samples: number of records
    :param n_classes: number of labels
    :param n_clusters: number of clusters per label
    :return: dataset and its labels in form of pandas DataFrame
    """
    assert type(n_samples) == int, "n_samples is not int"
    assert type(n_classes) == int, "n_classes is not int"
    assert type(n_clusters) == int, "n_clusters is not int"
    assert n_samples >= 1, "n_samples < 1"
    assert n_classes >= 1, "n_classes < 1"
    assert n_clusters >= 1, "n_clusters < 1"

    data, labels = make_classification(n_samples=n_samples,
                                       n_features=2,
                                       n_informative=2,
                                       n_redundant=0,
                                       n_classes=n_classes,
                                       n_clusters_per_class=n_clusters)

    return pd.DataFrame(data), pd.DataFrame(labels)


def create_3D_set(n_samples=1000, n_classes=3, n_clusters=1):
    """
    Generate simple dataset with three features
    :param n_samples: number of records
    :param n_classes: number of labels
    :param n_clusters: number of clusters per label
    :return: dataset and its labels in form of pandas DataFrame
    """
    assert type(n_samples) == int, "n_samples is not int"
    assert type(n_classes) == int, "n_classes is not int"
    assert type(n_clusters) == int, "n_clusters is not int"
    assert n_samples >= 1, "n_samples < 1"
    assert n_classes >= 1, "n_classes < 1"
    assert n_clusters >= 1, "n_clusters < 1"

    data, labels = make_classification(n_samples=n_samples,
                                       n_features=3,
                                       n_informative=3,
                                       n_redundant=0,
                                       n_classes=n_classes,
                                       n_clusters_per_class=n_clusters)

    return pd.DataFrame(data), pd.DataFrame(labels)


def train_test_from_cross_val_data(cross_val_data, which_test):
    train_x = []
    train_y = []
    test_x = []
    test_y = []
    for i in range(len(cross_val_data)):
        if i != which_test:
            train_x.append(cross_val_data[i][0])
            train_y.append(cross_val_data[i][1])
        else:
            test_x.append(cross_val_data[i][0])
            test_y.append(cross_val_data[i][1])

    return pd.concat(train_x), pd.concat(train_y), pd.concat(test_x), pd.concat(test_y)


def do_benchmark_knn(classifier, cross_val_data, neighbors=5):
    err = []
    acc = []
    for i in range(len(cross_val_data)):
        train_x, train_y, test_x, test_y = train_test_from_cross_val_data(cross_val_data, i)
        y_pred = classifier(train_x, train_y, test_x, neighbors)
        err.append(calculate_error(test_y, y_pred))
        acc.append(calculate_accuracy(test_y, y_pred))
    return err, acc


def do_benchmark(classifier, cross_val_data):
    err = []
    acc = []
    for i in range(len(cross_val_data)):
        train_x, train_y, test_x, test_y = train_test_from_cross_val_data(cross_val_data, i)
        classifier.fit(train_x, train_y.values.ravel())
        y_pred = classifier.predict(test_x)
        err.append(calculate_error(test_y, y_pred))
        acc.append(calculate_accuracy(test_y, y_pred))
    return err, acc


def plot_benchmarks(knn_res, random_res, ada_res, lda_res, result="Accuracy"):
    names = ["KNN", "RandomForestClassifier", "AdaBoostClassifier", "LinearDiscriminantAnalysis"]

    res = knn_res + random_res + ada_res + lda_res
    results_df = []

    for i in range(len(knn_res)*4):
        results_df.append([names[int(i/5)], i%5, res[i]])

    # Create the pandas DataFrame
    df = pd.DataFrame(results_df, columns=['Name', 'CV', result])

    ax = sns.barplot(x=result, y="Name", hue="CV", data=df, palette="Blues_d")
    plt.xlim(0, 1)
    plt.legend(fontsize='small')
    plt.show()


def plot_benchmarks5(knn_res3, knn_res6, random_res, ada_res, lda_res, result="Accuracy"):
    names = ["KNN-3", "KNN-6", "RandomForestClassifier", "AdaBoostClassifier", "LinearDiscriminantAnalysis"]

    res = knn_res3 + knn_res6 +  random_res + ada_res + lda_res
    results_df = []

    for i in range(len(knn_res3)*5):
        results_df.append([names[int(i/5)], i % 5, res[i]])

    # Create the pandas DataFrame
    df = pd.DataFrame(results_df, columns=['Name', 'CV', result])

    ax = sns.barplot(x=result, y="Name", hue="CV", data=df, palette="Blues_d")
    plt.xlim(0, 1)
    plt.legend(fontsize='small')
    plt.show()

    return df


def benchmark_to_csv(dst_folder, ben_name, ben_data_acc, ben_data_mean):
    ben_data_acc.to_csv(os.path.join(dst_folder, ben_name + "_acc.csv"), index=False)
    ben_data_mean.to_csv(os.path.join(dst_folder, ben_name + "_mean.csv"), index=False)


def load_benchmarks(src_folder, ben_name):
    ben_data_acc = pd.read_csv(os.path.join(src_folder, ben_name + "_acc.csv"))
    ben_data_mean = pd.read_csv(os.path.join(src_folder, ben_name + "_mean.csv"))

    return ben_data_acc, ben_data_mean

def print_benchmarks(ben_data_acc, ben_data_mean, result="Accuracy"):
    ax = sns.barplot(x=result, y="Name", hue="CV", data=ben_data_acc, palette="Blues_d")
    plt.xlim(0, 1)
    plt.legend(fontsize='small')
    plt.show()

    return ben_data_mean


def table_acc_err(knn_res, random_res, ada_res, lda_res, knn_err, random_err, ada_err, lda_err):
    names = ["KNN", "RandomForestClassifier", "AdaBoostClassifier", "LinearDiscriminantAnalysis"]
    res = [np.mean(knn_res), np.mean(random_res), np.mean(ada_res), np.mean(lda_res)]
    err = [np.mean(knn_err), np.mean(random_err), np.mean(ada_err), np.mean(lda_err)]
    acc = sorted(list(zip(names, res, err)), key=lambda x: -x[1])

    df = pd.DataFrame(acc, columns=['Name', "Mean Accuracy", "Mean Error"])

    return df

def table_acc_err5(knn_res3, knn_res6, random_res, ada_res, lda_res, knn_err3, knn_err6, random_err, ada_err, lda_err):
    names = ["KNN-3", "KNN-6", "RandomForestClassifier", "AdaBoostClassifier", "LinearDiscriminantAnalysis"]
    res = [np.mean(knn_res3), np.mean(knn_res6), np.mean(random_res), np.mean(ada_res), np.mean(lda_res)]
    err = [np.mean(knn_err3), np.mean(knn_err6), np.mean(random_err), np.mean(ada_err), np.mean(lda_err)]
    acc = sorted(list(zip(names, res, err)), key=lambda x: -x[1])

    df = pd.DataFrame(acc, columns=['Name', "Mean Accuracy", "Mean Error"])

    return df


# TODO: Sprawdzic na jakis binarnych problemach
def perf_measure(y_actual, y_hat):
    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for i in range(len(y_hat)):
        if y_actual[i]==y_hat[i]==1:
           TP += 1
        if y_hat[i]==1 and y_actual[i]!=y_hat[i]:
           FP += 1
        if y_actual[i]==y_hat[i]==0:
           TN += 1
        if y_hat[i]==0 and y_actual[i]!=y_hat[i]:
           FN += 1

    return(TP, FP, TN, FN)


